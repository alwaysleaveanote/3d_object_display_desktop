﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

//This script handles the functionality of adding a new label to the main object
public class AddLabelOnClick : MonoBehaviour
{
    public GameObject m_bubble = null;
    private GameObject sceneController = null;
    private GameObject dataBase = null;

    private bool clickable = false;

    private MessageHandler messageHandler = null;


    void Start()
    {
        sceneController = GameObject.Find("SceneController");
        dataBase = GameObject.Find("DataBase");
        messageHandler = GameObject.Find("MessageHandler").GetComponent<MessageHandler>();
        m_bubble = GameObject.Find("Bubble");
    }
    //when left mouse click occurs, test if input is valid and add a new label
    void OnMouseDown()
    {
        clickable = sceneController.GetComponent<SceneController>().clickable;
        if (clickable)
        {
            if (!EventSystem.current.IsPointerOverGameObject())
            {
                RaycastHit hit;
                Vector3 clickPosition = Input.mousePosition;
                Ray ray = Camera.main.ScreenPointToRay(clickPosition);
                if (Physics.Raycast(ray, out hit, 100.0f))
                {
                    string title = sceneController.GetComponent<SceneController>().titleField.text;
                    string description = sceneController.GetComponent<SceneController>().infoField.text;
                    if (!dataBase.GetComponent<DataBase>().KeyAlreadyExists(title))
                    {
                        if (title == "")
                        {
                            messageHandler.PrintMessageTemp("The label title field cannot be empty");
                            //error message that title and description cant be empty
                        }
                        else
                        {
                            GameObject bubble = Instantiate(m_bubble);

                            //put the newly created bubble on the gameobject at the root parent
                            bubble.transform.position = hit.point;
                            bubble.transform.SetParent(hit.transform.parent.parent);
                            bubble.gameObject.name = sceneController.GetComponent<SceneController>().titleField.text;

                            dataBase.GetComponent<DataBase>().AddToDictionary(title, description);

                            sceneController.GetComponent<SceneController>().clickable = false;
                            sceneController.GetComponent<SceneController>().titleField.text = "";
                            sceneController.GetComponent<SceneController>().infoField.text = "";
                            sceneController.GetComponent<SceneController>().titleField.gameObject.SetActive(false);
                            sceneController.GetComponent<SceneController>().infoField.gameObject.SetActive(false);
                            sceneController.GetComponent<SceneController>().addButton.gameObject.SetActive(true);
                            sceneController.GetComponent<SceneController>().disableNewLabelButton.gameObject.SetActive(false);
                        }
                    }
                    else
                    {
                        messageHandler.PrintMessageTemp("Label titles must be unique");
                        //error mesage that titles must be unique
                    }
                }
            }
        }
    }
}
