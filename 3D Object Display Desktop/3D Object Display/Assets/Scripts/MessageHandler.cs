﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//This script is used to temporarily print a message on the screen for the user to see. alot of this is nolonger used and should probably be phased out.
public class MessageHandler : MonoBehaviour
{
    [SerializeField]
    GameObject message = null;

    //prints a message on the screen that lasts 2 seconds.
    public void PrintMessageTemp(string info)
    {
        message.GetComponent<Image>().CrossFadeAlpha(255f, .25f, false);
        message.GetComponentInChildren<Text>().CrossFadeAlpha(255f, .25f, false);

        message.GetComponentInChildren<UnityEngine.UI.Text>().text = info;
        Invoke("DisableText", 2f);
        
    }
    //prints a message to the screen that exist until remove is called.
    public void PrintMessage(string info)
    {
        message.GetComponent<Image>().CrossFadeAlpha(255f, .25f, false);
        message.GetComponentInChildren<Text>().CrossFadeAlpha(255f, .25f, false);
        message.GetComponentInChildren<UnityEngine.UI.Text>().text = info;
    }
    //removes a printed message
    public void RemoveMessage()
    {
        message.GetComponent<Image>().CrossFadeAlpha(0.1f, .25f, false);
        message.GetComponentInChildren<Text>().CrossFadeAlpha(0.1f, .25f, false);
    }
    private void DisableText()
    {
        message.GetComponent<Image>().CrossFadeAlpha(0.1f, 2.0f, false);
        message.GetComponentInChildren<Text>().CrossFadeAlpha(0.1f, 1f, false);
        //message.SetActive(false);
    }
}
