﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

//In general, this script manages what panels and fields are visible and when.
public class SceneController : MonoBehaviour
{
    
    public InputField titleField = null;
    
    public InputField infoField = null;
   
    public Button addButton = null;

    [SerializeField]
    Button exitButton = null;
    [SerializeField]
    Button fullscreenButton = null;
    [SerializeField]
    Button windowedButton = null;

    [SerializeField]
    Button saveButton = null;
    [SerializeField]
    Button deleteButton = null;

    [SerializeField]
    Button enableEditButton = null;
    [SerializeField]
    Button disableEditButton = null;

    public Button disableNewLabelButton = null;

    [SerializeField]
    Button growWindowButton = null;
    [SerializeField]
    Button shrinkWindowButton = null;

    [SerializeField]
    Button saveSceneButton = null;

    [SerializeField]
    GameObject guidePanel = null;

    [SerializeField]
    GameObject labelPanel = null;

    [SerializeField]
    Button editPanel = null;

    [SerializeField]
    GameObject spherePointer = null;

    [SerializeField]
    GameObject objectLoaderButton = null;

    [SerializeField]
    GameObject newObjectButton = null;

    private bool guideOpen = false;
    private bool toggledOff = false;
    private bool editToggledOff = false;

    // ALysia added this
    [SerializeField]
    GameObject bubbleIconOff = null;
    [SerializeField]
    GameObject bubbleIcon = null;

    [HideInInspector]
    public bool clickable = false;

    private GameObject scripts = null;
    private MoveObj moveObj = null;

    private ClickManager clickManager = null;
    private DataBase dataBase = null;

    private string currentBubbleName = null;


    // initilize all panels and buttons on screen
    void Start()
    {
        Screen.SetResolution(Screen.width + 2000, Screen.height + 2000, false, 0);
        //Debug.Log("START - Width: " + Screen.width + "  Height: " + Screen.height);

        enableEditButton.gameObject.SetActive(true);
        disableEditButton.gameObject.SetActive(false);

        disableNewLabelButton.gameObject.SetActive(false);

        saveButton.gameObject.SetActive(false);
        deleteButton.gameObject.SetActive(false);

        infoField.gameObject.SetActive(false);

        titleField.gameObject.SetActive(false);
        infoField.gameObject.SetActive(false);
        addButton.gameObject.SetActive(true);

        addButton.gameObject.SetActive(false);
        saveSceneButton.gameObject.SetActive(false);

        saveButton.gameObject.SetActive(false);
        deleteButton.gameObject.SetActive(false);

        guidePanel.SetActive(false);
        guideOpen = false;
        clickable = false;

        exitButton.gameObject.SetActive(true);

        shrinkWindowButton.gameObject.SetActive(false);
        growWindowButton.gameObject.SetActive(false);

        windowedButton.gameObject.SetActive(true);
        fullscreenButton.gameObject.SetActive(false);
        Screen.fullScreen = true;

        // Alysia added this
        bubbleIconOff.gameObject.SetActive(false);
        bubbleIcon.gameObject.SetActive(true);

        scripts = GameObject.Find("Scripts");
        moveObj = scripts.GetComponent<MoveObj>();

        clickManager = GameObject.Find("ClickManager").GetComponent<ClickManager>();
        dataBase = GameObject.Find("DataBase").GetComponent<DataBase>();
    }
    //determine if the object can be moved or not depending on what is on screen, this needs to be updated since new panels were added to the scene.
    void Update()
    {
        if (titleField.isFocused || infoField.isFocused)
        {
            moveObj.canMove = false;
        }
        else
        {
            moveObj.canMove = true;
        }
    }
    //set application to full screen
    public void ToggleFullscreen()
    {
        if (Screen.fullScreen == true)  // Turn off fullscreen
        {
            exitButton.gameObject.SetActive(false);
            windowedButton.gameObject.SetActive(false);
            fullscreenButton.gameObject.SetActive(true);
            shrinkWindowButton.gameObject.SetActive(true);
            growWindowButton.gameObject.SetActive(true);
            Screen.SetResolution(Screen.width - 200, Screen.height - 100, false, 0);
            //Debug.Log("OFF FULL - Width: " + Screen.width + "  Height: " + Screen.height);

        }
        else    // Switch to fullscreen
        {
            exitButton.gameObject.SetActive(true);
            windowedButton.gameObject.SetActive(true);
            fullscreenButton.gameObject.SetActive(false);
            shrinkWindowButton.gameObject.SetActive(false);
            growWindowButton.gameObject.SetActive(false);
            Screen.SetResolution(Screen.width + 2000, Screen.height + 2000, false, 0);
            //Debug.Log("FULLSCREEN - Width: " + Screen.width + "  Height: " + Screen.height);
        }

        Screen.fullScreen = !Screen.fullScreen;
    }
    //close the application
    public void ExitProgram()
    {
        Application.Quit();
    }
    //start process for adding a new label to an object
    public void BeginAddLabel()
    {
        disableNewLabelButton.gameObject.SetActive(true);
        titleField.gameObject.SetActive(true);
        infoField.gameObject.SetActive(true);
        titleField.text = "";
        infoField.text = "";
        addButton.gameObject.SetActive(false);
        

        clickable = true;
    }
    //open the help guide
    public void ToggleGuide()
    {
        if(guideOpen == false)
        {
            guidePanel.SetActive(true);
            guideOpen = true;
        }
        else
        {
            guidePanel.SetActive(false);
            guideOpen = false;
        }
    }
    //make windowed appliaction larger
    public void GrowWindow()
    {
        Screen.SetResolution(Screen.width + 200, Screen.height + 100, false, 0);
        Debug.Log("Width: " + Screen.width + "  Height: " + Screen.height);
    }
    //make windowed appliaction smaller
    public void ShrinkWindow()
    {
        Screen.SetResolution(Screen.width - 200, Screen.height - 100, false, 0);
        Debug.Log("Width: " + Screen.width + "  Height: " + Screen.height);
    }
    //allow for editing of a label
    public void EnableEditLabel()
    {
        if(!editToggledOff)
        {
            MessageHandler messageHandler = GameObject.Find("MessageHandler").GetComponent<MessageHandler>();
            messageHandler.PrintMessageTemp("Must be in edit mode to edit labels");
        }
        else
        {
            titleField.gameObject.SetActive(true);
            infoField.gameObject.SetActive(true);
            saveButton.gameObject.SetActive(true);
            deleteButton.gameObject.SetActive(true);
            addButton.gameObject.SetActive(false);

            currentBubbleName = clickManager.lastClickedPartname;
            clickManager.panel.SetActive(false);
            clickManager.theLineDrawer.setObjectToPointAt(null, clickManager.panel.GetComponent<RectTransform>().position);

            infoField.text = dataBase.getText(currentBubbleName);
            titleField.text = currentBubbleName;
        }
    }
    //save after making a change to a label and store that change in the database
    public void SaveLabelChanges()
    {
        MessageHandler messageHandler = GameObject.Find("MessageHandler").GetComponent<MessageHandler>();

        if (dataBase.KeyAlreadyExists(titleField.text) && titleField.text != currentBubbleName)
        {
            messageHandler.PrintMessageTemp("Label titles must be unique");
        }
        else
        {
            if (titleField.text != "")
            {
                dataBase.RemoveDicValue(currentBubbleName);
                GameObject.Find(currentBubbleName).name = titleField.text;
                currentBubbleName = titleField.text;

                dataBase.AddToDictionary(currentBubbleName, infoField.text);

                infoField.gameObject.SetActive(false);
                saveButton.gameObject.SetActive(false);
                deleteButton.gameObject.SetActive(false);
                titleField.gameObject.SetActive(false);
                if (editToggledOff)
                {
                    addButton.gameObject.SetActive(true);
                }
            }
            else
            {
                messageHandler.PrintMessageTemp("The label title field cannot be empty");
            }
        }
    }
    //delete an existing label
    public void DeleteLabel()
    {
        dataBase.RemoveDicValue(currentBubbleName);
        spherePointer.transform.SetParent(GameObject.Find("New Game Object").transform);
        Destroy(GameObject.Find(currentBubbleName));

        titleField.gameObject.SetActive(false);
        infoField.gameObject.SetActive(false);
        saveButton.gameObject.SetActive(false);
        deleteButton.gameObject.SetActive(false);
        addButton.gameObject.SetActive(true);
    }
    //hide labels from view
    public void ToggleLabelView()
    {
        GameObject m_object = GameObject.Find("New Game Object");
        if(toggledOff == false)
        {
            clickManager.panel.SetActive(false);
            clickManager.theLineDrawer.setObjectToPointAt(null, clickManager.panel.GetComponent<RectTransform>().position);
            foreach (Transform child in m_object.transform)
            {
                foreach (Transform baby in child)
                {
                    if (dataBase.getText(baby.gameObject.name) != null)
                    {

                        baby.gameObject.SetActive(false);
                    }
                }
                    if (dataBase.getText(child.gameObject.name) != null)
                {
                    
                    child.gameObject.SetActive(false);
                }
            }
            toggledOff = true;

            // Alysia added this
            bubbleIcon.gameObject.SetActive(false);
            bubbleIconOff.gameObject.SetActive(true);
            //Debug.Log("turn off labels");
        }
        else
        {
            foreach (Transform child in m_object.transform)
            {
                foreach (Transform baby in child)
                {
                    if (dataBase.getText(baby.gameObject.name) != null)
                    {

                        baby.gameObject.SetActive(true);
                    }
                }
                child.gameObject.SetActive(true);
            }
            toggledOff = false;

            // Alysia added this
            bubbleIcon.gameObject.SetActive(true);
            bubbleIconOff.gameObject.SetActive(false);
            //Debug.Log("turn on labels");
        }
    }
    //toggle editing
    public void ToggleEditMode()
    {      
        if (editToggledOff == false)
        {
            addButton.gameObject.SetActive(true);
            saveSceneButton.gameObject.SetActive(true);
            enableEditButton.gameObject.SetActive(false);
            disableEditButton.gameObject.SetActive(true);
            editToggledOff = true;
        }
        else
        {
            addButton.gameObject.SetActive(false);
            disableNewLabelButton.gameObject.SetActive(false);
            saveSceneButton.gameObject.SetActive(false);
            enableEditButton.gameObject.SetActive(true);
            disableEditButton.gameObject.SetActive(false);
            titleField.gameObject.SetActive(false);
            infoField.gameObject.SetActive(false);
            saveButton.gameObject.SetActive(false);
            deleteButton.gameObject.SetActive(false);
            editToggledOff = false;
        }
    }
    //stop editing or adding a label
    public void DisableNew()
    {
        disableNewLabelButton.gameObject.SetActive(false);
        titleField.gameObject.SetActive(false);
        infoField.gameObject.SetActive(false);
        saveButton.gameObject.SetActive(false);
        deleteButton.gameObject.SetActive(false);
        addButton.gameObject.SetActive(true);
        clickable = false;
    }
    //reloads the scene so new objects can be loaded
    public void Reload()
    {
        SceneManager.LoadScene("Main");
    }
    //disables ability to load new object in current scene, scene must be reloaded
    public void LoadClicked()
    {
        objectLoaderButton.SetActive(false);
        newObjectButton.SetActive(true);
    }
}
