﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BubbleDropdown : MonoBehaviour
{
    List<string> colorNames = new List<string>() { "Red", "Green", "Blue", "Yellow"};
    List<Color> colors;

    public Material mat;
    public Dropdown dropdown;
    public Image icon;

    // Start is called before the first frame update
    void Start()
    {
        Color red = new Color(.75f, 0, 0, .3f);
        Color green = new Color(0, .75f, 0, .3f);
        Color blue = new Color(0,0,.75f,.3f);
        Color yellow = new Color(1f,.75f,.1f,.3f);
        

        colors = new List<Color>() {red,green,blue,yellow };
        PopulateList();
        mat.color = red;
        icon.color = red;
    }
    public void ChangeBubble(int index)
    {
        mat.color = colors[index];
        icon.color = colors[index];
    }

    void PopulateList()
    {
        dropdown.AddOptions(colorNames);
    }
}
