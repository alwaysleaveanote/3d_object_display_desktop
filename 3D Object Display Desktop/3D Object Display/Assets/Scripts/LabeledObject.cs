﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//This script is responsible for getting the object ready for and performing saving and loading.
public class LabeledObject : MonoBehaviour
{
    [HideInInspector]
    public string[] titles;
    [HideInInspector]
    public string[] descriptions;
    [HideInInspector]
    public float[][] labels;
    [HideInInspector]
    public float[] objRotation;
    [HideInInspector]
    public float objZPosition;
    private DataBase dataBase = null;
    private Dictionary<string, string> dic = new Dictionary<string, string>();
    [SerializeField]
    GameObject bubble = null;
    private GameObject m_object = null;
    private MessageHandler messageHandler = null;
    [HideInInspector]
    public string m_path = null;
    [HideInInspector]
    public ObjData objData = null;
    public bool readyToLoad = false;

    void Start()
    {
        dataBase = GameObject.Find("DataBase").GetComponent<DataBase>();
        messageHandler = GameObject.Find("MessageHandler").GetComponent<MessageHandler>();
    }

    //wait until an object exists in the scene, then store the object for later use. This is not a good way of doing this and should be changed in the future.
    private void Update()
    {
        m_object = GameObject.Find("New Game Object");
        if (readyToLoad && m_object != null)
        {
            LoadObject();
            readyToLoad = false;
        }
    }

    //save the objects rotation, position, and labels.
    public void SaveObject()
    {
        messageHandler.PrintMessageTemp("Saving...");
        convertToSavable();

        SaveSystem.SaveObject(this, m_path);
    }

    //converts the object's save data into useable data
    public void LoadObject()
    {
        if (m_path != null && m_path != "")
        {
            ObjData objData = SaveSystem.LoadObject(m_path);

            titles = objData.titles;
            descriptions = objData.descriptions;
            labels = objData.labels;
            objRotation = objData.objRotation;
            objZPosition = objData.objZPosition;

            convertFromSavable();
        }
        else
        {
            titles = objData.titles;
            descriptions = objData.descriptions;
            labels = objData.labels;
            objRotation = objData.objRotation;
            objZPosition = objData.objZPosition;
            
            convertFromSavable();
        }
    }
    //convert all object data to data we can save to a file.
    private void convertToSavable()
    {
        
        dic = dataBase.GetDictionary();

        m_object = GameObject.Find("New Game Object");

        objZPosition = m_object.transform.position.z;
        Vector3 objRot = m_object.transform.rotation.eulerAngles;

        objRotation = new float[3];
        objRotation[0] = objRot.x;
        objRotation[1] = objRot.y;
        objRotation[2] = objRot.z;

        titles = new string[dic.Count];
        descriptions = new string[dic.Count];
        labels = new float[dic.Count][];

        int itr = 0;
        foreach (KeyValuePair < string,string > bubble in dic)
        {
            titles[itr] = bubble.Key;
            descriptions[itr] = bubble.Value;

            GameObject currentBubble = GameObject.Find(titles[itr]);
            Vector3 bubPosition = currentBubble.transform.position;

            float[] pos = new float[3];
            pos[0] = bubPosition.x;
            pos[1] = bubPosition.y;
            pos[2] = bubPosition.z;


            labels[itr] = pos;
            itr++;
        }
    }

    //takes the previously converted data and manipluate the object and it's labels to become how it was when last saved.
    private void convertFromSavable()
    {
        m_object = GameObject.Find("New Game Object");
        Vector3 objectRotation = new Vector3();
        objectRotation.x = objRotation[0];
        objectRotation.y = objRotation[1];
        objectRotation.z = objRotation[2];

        Vector3 objectPosition = new Vector3();
        objectPosition.x = m_object.transform.position.x;
        objectPosition.y = m_object.transform.position.y;
        objectPosition.z = objZPosition;

        m_object.transform.position  = objectPosition;
        m_object.transform.eulerAngles = objectRotation;

        for (int i = 0; i < titles.Length; i++)
        {
            dataBase.AddToDictionary(titles[i], descriptions[i]);

            Vector3 labelPosition = new Vector3();
            labelPosition.x = labels[i][0];
            labelPosition.y = labels[i][1];
            labelPosition.z = labels[i][2];

            GameObject currentBubble = Instantiate(bubble, labelPosition, Quaternion.identity);
            currentBubble.name = titles[i];
            
            currentBubble.transform.SetParent(m_object.transform);
        }
    }
}
