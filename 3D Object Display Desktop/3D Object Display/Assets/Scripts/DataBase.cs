﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//The database script is stores all data on the labels of the loaded object.
public class DataBase : MonoBehaviour {

    // The Database containing a serie of 1 object Name, 1 description, 1 object Name, 1 description,...
    [HideInInspector]
    public string[] database;

    private Dictionary<string, string> dic = new Dictionary<string, string>();

	// Use this for initialization
	void Start () {
        // Fill the dictionnary with database parameter
		for (int i = 0; i < database.Length; i++)
        {
            dic.Add(database[i], database[i + 1]);
            i++;
        }
	}
    //add a label to the dictionary
    public void AddToDictionary(string title, string description)
    {
        if(!dic.ContainsKey(title))
        {
            dic.Add(title, description);
        }
    }
    //change a label
    public void EditDicValue(string title, string description)
    {
        dic[title] = description;
    }
    //remove a label
    public void RemoveDicValue(string title)
    {
        dic.Remove(title);
    }

    // Returns the description associated to the object name.
    public string getText(string name)
    {
        if(dic.ContainsKey(name))
        {
            return dic[name];
        }
        else
        {
            return null;
        }
        
    }
    //test to see if label name already exists, labels should be unique
    public bool KeyAlreadyExists(string key)
    {
        return dic.ContainsKey(key);
    }
    //get the dictionary
    public Dictionary<string, string> GetDictionary()
    {
        return dic;
    }
}
