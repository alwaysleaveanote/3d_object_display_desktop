﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

//This script deals with externally saving the object and its labels and loading them from an external file.
public class SaveSystem : MonoBehaviour
{
    [HideInInspector]
    public static string parentDirectory = null;
    
    //saves the objects relevant data to an external file
    public static void SaveObject(LabeledObject m_object, string m_path)
    {
        GetParentDir(m_path);

        BinaryFormatter formatter = new BinaryFormatter();
        string path = parentDirectory + "/Object.savedata";

        FileStream stream = new FileStream(path, FileMode.Create);

        ObjData objData = new ObjData(m_object);

        formatter.Serialize(stream, objData);
        stream.Close();
    }
    //loads an objects relavant data from an external file
    public static ObjData LoadObject(string m_path)
    {
        GetParentDir(m_path);

        string path = parentDirectory + "/Object.savedata";
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            ObjData objData = formatter.Deserialize(stream) as ObjData;
            stream.Close();

            return objData;
        }
        else
        {
            //error message no file path
            return null;
        }

    }
    //gets that folder the object is stored in externally
    public static void GetParentDir(string m_path)
    {
        DirectoryInfo parentDir = Directory.GetParent(m_path);
        parentDirectory = parentDir.FullName;
    }
}
