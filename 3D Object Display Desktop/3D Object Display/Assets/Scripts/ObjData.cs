﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//This script is used for saving and loading the main object. This is the stored data that gets saved to a file when save is called.
[System.Serializable]
public class ObjData
{
    public string[] titles;
    public string[] descriptions;
    public float[] objRotation;
    public float[][] labels;
    public float objZPosition;

    public ObjData(LabeledObject m_object)
    {
        titles = m_object.titles;
        descriptions = m_object.descriptions;
        labels = m_object.labels;
        objRotation = m_object.objRotation;
        objZPosition = m_object.objZPosition;
    }
    
}
