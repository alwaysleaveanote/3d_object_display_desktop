﻿#pragma warning disable 649
using System;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.UI;
using Debug = UnityEngine.Debug;

namespace TriLib
{
    namespace Samples
    {
        /// <summary>
        /// Represents the asset loader UI component.
        /// </summary>
        [RequireComponent(typeof(AssetDownloader))]
        public class AssetLoaderWindow : MonoBehaviour
        {
            /// <summary>
            /// Class singleton.
            /// </summary>
            public static AssetLoaderWindow Instance { get; private set; }

            /// <summary>
            /// Turn on this field to enable async loading.
            /// </summary>
            public bool Async;

            /// <summary>
            /// "Load local asset button" reference.
            /// </summary>
            [SerializeField]
            private UnityEngine.UI.Button _loadLocalAssetButton;
            /// <summary>
            /// "Load remote button" reference.
            /// </summary>
            [SerializeField]
            private UnityEngine.UI.Button _loadRemoteAssetButton;
            
            /// <summary>
            /// "Background (gradient) canvas" reference.
            /// </summary>
            [SerializeField]
            private Canvas _backgroundCanvas;
            /// <summary>
            /// Loaded Game Object reference.
            /// </summary>
            private GameObject _rootGameObject;
            /// <summary>
            /// Total loading time Text reference.
            /// </summary>
            [SerializeField]
            private Text _loadingTimeText;

            /// <summary>
            /// WebGL drag and drop Text reference.
            /// </summary>
            [SerializeField]
            private Text _dragAndDropText;

            /// <summary>
            /// Loading timer.
            /// </summary>
            private Stopwatch _loadingTimer = new Stopwatch();

            /// <summary>
            /// Handles events from <see cref="AnimationText"/>.
            /// </summary>
            /// <param name="animationName">Chosen animation name.</param>
            public void HandleEvent(string animationName)
            {
                _rootGameObject.GetComponent<Animation>().Play(animationName);
            }

            /// <summary>
            /// Handles events from <see cref="BlendShapeControl"/>.
            /// </summary>
            /// <param name="skinnedMeshRenderer">Skinned Mesh Renderer to set the blend shape.</param>
            /// <param name="index">Blend Shape index.</param>
            /// <param name="value">Blend Shape value.</param>
            public void HandleBlendEvent(SkinnedMeshRenderer skinnedMeshRenderer, int index, float value)
            {
                skinnedMeshRenderer.SetBlendShapeWeight(index, value);
            }

            /// <summary>
            /// Initializes variables.
            /// </summary>
            protected void Awake()
            {
                _loadLocalAssetButton.onClick.AddListener(LoadLocalAssetButtonClick);
#if !UNITY_EDITOR && UNITY_WEBGL
                _dragAndDropText.gameObject.SetActive(true);
#endif
                HideControls();
                Instance = this;
            }

            /// <summary>
            /// Hides user controls.
            /// </summary>
            private void HideControls()
            {
                _loadLocalAssetButton.interactable = true;
            }

            /// <summary>
            /// Handles "Load local asset button" click event and tries to load an asset at chosen path.
            /// </summary>
            private void LoadLocalAssetButtonClick()
            {
#if !UNITY_EDITOR && UNITY_WEBGL
                
                ErrorDialog.Instance.ShowDialog("Please drag and drop your model files into the browser window.");
#else
                var fileOpenDialog = FileOpenDialog.Instance;
                fileOpenDialog.Title = "Please select a File";
                fileOpenDialog.Filter = AssetLoaderBase.GetSupportedFileExtensions() + ";*.zip;";
#if !UNITY_EDITOR && UNITY_WINRT && (NET_4_6 || NETFX_CORE || NET_STANDARD_2_0)
                fileOpenDialog.ShowFileOpenDialog(delegate (byte[] fileBytes, string filename) 
                {
                    LoadInternal(filename, fileBytes);
#else
                fileOpenDialog.ShowFileOpenDialog(delegate (string filename)
                {
                    LoadInternal(filename);
#endif
                }
                );
#endif
            }

            //#if !UNITY_EDITOR && UNITY_WEBGL
            /// <summary>
            /// Loads the model from browser files.
            /// </summary>
            /// <param name="filesCount">Browser files count.</param>
            public void LoadFromBrowserFiles(int filesCount)
            {
                LoadInternal(null, null, filesCount);
            }
            //#endif

            /// <summary>
            /// Executes the post model loading steps.
            /// </summary>
            private void FullPostLoadSetup()
            {
                if (_rootGameObject != null)
                {
                    PostLoadSetup();
                    ShowLoadingTime();
                }
                else
                {
                    HideLoadingTime();
                }
            }

            /// <summary>
            /// Displays a <see cref="System.Exception"/>.
            /// </summary>
            /// <param name="exception"><see cref="System.Exception"/> to display.</param>
            private void HandleException(Exception exception)
            {
                if (_rootGameObject != null)
                {
                    Destroy(_rootGameObject);
                }
                _rootGameObject = null;
                HideLoadingTime();
                ErrorDialog.Instance.ShowDialog(exception.ToString());
            }

            /// <summary>
            /// Checks for a valid model (a model should contain meshes to be displayed).
            /// </summary>
            /// <param name="assetLoader"><see cref="AssetLoaderBase"/> used to load the model.</param>
            private void CheckForValidModel(AssetLoaderBase assetLoader)
            {
                if (assetLoader.MeshData == null || assetLoader.MeshData.Length == 0)
                {
                    throw new Exception("File contains no meshes");
                }
            }

            /// <summary>
            /// Loads a model from the given filename, file bytes or browser files.
            /// </summary>
            /// <param name="filename">Model filename.</param>
            /// <param name="fileBytes">Model file bytes.</param>
            /// <param name="browserFilesCount">Browser files count.</param>
            private void LoadInternal(string filename, byte[] fileBytes = null, int browserFilesCount = -1)
            {
                PreLoadSetup();
                var assetLoaderOptions = GetAssetLoaderOptions();
                if (!Async)
                {
                    using (var assetLoader = new AssetLoader())
                    {
                        assetLoader.OnMetadataProcessed += AssetLoader_OnMetadataProcessed;
                        try
                        {
#if !UNITY_EDITOR && UNITY_WEBGL
                            if (browserFilesCount >= 0)
                            {
                                _rootGameObject = assetLoader.LoadFromBrowserFilesWithTextures(browserFilesCount, assetLoaderOptions);
                            }
                            else
#endif
                            if (fileBytes != null && fileBytes.Length > 0)
                            {
                                _rootGameObject = assetLoader.LoadFromMemoryWithTextures(fileBytes, FileUtils.GetFileExtension(filename), assetLoaderOptions, _rootGameObject);
                            }
                            else if (!string.IsNullOrEmpty(filename))
                            {
                                _rootGameObject = assetLoader.LoadFromFileWithTextures(filename, assetLoaderOptions);
                            }
                            else
                            {
                                throw new Exception("File not selected");
                            }
                            CheckForValidModel(assetLoader);
                        }
                        catch (Exception exception)
                        {
                            HandleException(exception);
                        }
                    }
                    FullPostLoadSetup();
                }
                else
                {
                    using (var assetLoader = new AssetLoaderAsync())
                    {
                        assetLoader.OnMetadataProcessed += AssetLoader_OnMetadataProcessed;
                        try
                        {
#if !UNITY_EDITOR && UNITY_WEBGL
                            if (browserFilesCount >= 0)
                            {
                                assetLoader.LoadFromBrowserFilesWithTextures(browserFilesCount, assetLoaderOptions, null, delegate (GameObject loadedGameObject)
                                {
                                    CheckForValidModel(assetLoader);
                                    _rootGameObject = loadedGameObject;
                                    FullPostLoadSetup();
                                });
                            }
                            else
#endif
                            if (fileBytes != null && fileBytes.Length > 0)
                            {
                                assetLoader.LoadFromMemoryWithTextures(fileBytes, FileUtils.GetFileExtension(filename), assetLoaderOptions, null, delegate (GameObject loadedGameObject)
                                {
                                    CheckForValidModel(assetLoader);
                                    _rootGameObject = loadedGameObject;
                                    FullPostLoadSetup();
                                });
                            }
                            else if (!string.IsNullOrEmpty(filename))
                            {
                                assetLoader.LoadFromFileWithTextures(filename, assetLoaderOptions, null, delegate (GameObject loadedGameObject)
                                {
                                    CheckForValidModel(assetLoader);
                                    _rootGameObject = loadedGameObject;
                                    FullPostLoadSetup();
                                });
                            }
                            else
                            {
                                throw new Exception("File not selected");
                            }
                        }
                        catch (Exception exception)
                        {
                            HandleException(exception);
                        }
                    }
                }
            }

            /// <summary>
            /// Shows the total asset loading time.
            /// </summary>
            private void ShowLoadingTime()
            {
                _loadingTimeText.text = string.Format("Loading time: {0:00}:{1:00}.{2:00}", _loadingTimer.Elapsed.Minutes, _loadingTimer.Elapsed.Seconds, _loadingTimer.Elapsed.Milliseconds / 10);
                _loadingTimer.Stop();
            }

            /// <summary>
            /// Hides the total asset loading time.
            /// </summary>
            private void HideLoadingTime()
            {
                _loadingTimeText.text = null;
            }

            /// <summary>
            /// Event assigned to FBX metadata loading. Editor debug purposes only.
            /// </summary>
            /// <param name="metadataType">Type of loaded metadata</param>
            /// <param name="metadataIndex">Index of loaded metadata</param>
            /// <param name="metadataKey">Key of loaded metadata</param>
            /// <param name="metadataValue">Value of loaded metadata</param>
            private void AssetLoader_OnMetadataProcessed(AssimpMetadataType metadataType, uint metadataIndex, string metadataKey, object metadataValue)
            {
                Debug.Log("Found metadata of type [" + metadataType + "] at index [" + metadataIndex + "] and key [" + metadataKey + "] with value [" + metadataValue + "]");
            }

            /// <summary>
            /// Gets the asset loader options.
            /// </summary>
            /// <returns>The asset loader options.</returns>
            private AssetLoaderOptions GetAssetLoaderOptions()
            {
                var assetLoaderOptions = AssetLoaderOptions.CreateInstance();
                assetLoaderOptions.DontLoadCameras = false;
                assetLoaderOptions.DontLoadLights = false;
                assetLoaderOptions.UseOriginalPositionRotationAndScale = true;
                assetLoaderOptions.DisableAlphaMaterials = true;
         
                assetLoaderOptions.AddAssetUnloader = true;
                assetLoaderOptions.AdvancedConfigs.Add(AssetAdvancedConfig.CreateConfig(AssetAdvancedPropertyClassNames.FBXImportDisableDiffuseFactor, true));
                return assetLoaderOptions;
            }

            /// <summary>
            /// Pre Load setup.
            /// </summary>
            private void PreLoadSetup()
            {
                _loadingTimer.Reset();
                _loadingTimer.Start();
                HideControls();
                if (_rootGameObject != null)
                {
                    Destroy(_rootGameObject);
                    _rootGameObject = null;
                }
            }

            /// <summary>
            /// Post load setup.
            /// </summary>
            private void PostLoadSetup()
            {
                var mainCamera = Camera.main;
                mainCamera.FitToBounds(_rootGameObject.transform, 3f);
                var rootAnimation = _rootGameObject.GetComponent<Animation>();

                var skinnedMeshRenderers = _rootGameObject.GetComponentsInChildren<SkinnedMeshRenderer>();
                if (skinnedMeshRenderers != null)
                {
                    var hasBlendShapes = false;
                    foreach (var skinnedMeshRenderer in skinnedMeshRenderers)
                    {
                        if (!hasBlendShapes && skinnedMeshRenderer.sharedMesh.blendShapeCount > 0)
                        {
                            hasBlendShapes = true;
                        }
                    }
                }
            }

            /// <summary>
            /// Handles "Load remote asset button" click event and tries to load the asset at chosen URI.
            /// </summary>
            private void LoadRemoteAssetButtonClick()
            {
                URIDialog.Instance.ShowDialog(delegate (string assetUri, string assetExtension)
                {
                    var assetDownloader = GetComponent<AssetDownloader>();
                    assetDownloader.DownloadAsset(assetUri, assetExtension, LoadDownloadedAsset, null, GetAssetLoaderOptions());
                    _loadLocalAssetButton.interactable = false;
                    _loadRemoteAssetButton.interactable = false;
                });
            }

            /// <summary>
            /// Loads the downloaded asset.
            /// </summary>
            /// <param name="loadedGameObject">Loaded game object.</param>
            private void LoadDownloadedAsset(GameObject loadedGameObject)
            {
                PreLoadSetup();
                if (loadedGameObject != null)
                {
                    _rootGameObject = loadedGameObject;
                    PostLoadSetup();
                }
                else
                {
                    var assetDownloader = GetComponent<AssetDownloader>();
                    ErrorDialog.Instance.ShowDialog(assetDownloader.Error);
                }
            }
        }
    }
}
#pragma warning restore 649
