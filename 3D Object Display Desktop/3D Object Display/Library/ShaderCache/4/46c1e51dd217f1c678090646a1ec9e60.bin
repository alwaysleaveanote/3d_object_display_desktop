<Q                         SHADOWS_DEPTH      SHADOWS_SOFT   SPOT      _ALPHATEST_ON      _PARALLAXMAP   _SPECGLOSSMAP   8C  #ifdef VERTEX
#version 300 es

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	vec4 _WorldSpaceLightPos0;
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 unity_WorldTransformParams;
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _MainTex_ST;
uniform 	vec4 _DetailAlbedoMap_ST;
uniform 	mediump float _UVSec;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToLight[4];
in highp vec4 in_POSITION0;
in mediump vec3 in_NORMAL0;
in highp vec2 in_TEXCOORD0;
in highp vec2 in_TEXCOORD1;
in mediump vec4 in_TANGENT0;
out highp vec4 vs_TEXCOORD0;
out highp vec4 vs_TEXCOORD1;
out highp vec4 vs_TEXCOORD2;
out highp vec4 vs_TEXCOORD3;
out highp vec4 vs_TEXCOORD4;
out highp vec3 vs_TEXCOORD5;
out highp vec4 vs_TEXCOORD6;
out highp vec4 vs_TEXCOORD7;
out mediump vec3 vs_TEXCOORD8;
vec4 u_xlat0;
vec4 u_xlat1;
bool u_xlatb1;
vec4 u_xlat2;
vec3 u_xlat3;
mediump vec3 u_xlat16_4;
mediump vec3 u_xlat16_5;
mediump vec3 u_xlat16_6;
float u_xlat22;
mediump float u_xlat16_25;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[3] * in_POSITION0.wwww + u_xlat0;
    u_xlat2 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat2 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat2;
    u_xlat2 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat2;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat2;
#ifdef UNITY_ADRENO_ES3
    u_xlatb1 = !!(_UVSec==0.0);
#else
    u_xlatb1 = _UVSec==0.0;
#endif
    u_xlat1.xy = (bool(u_xlatb1)) ? in_TEXCOORD0.xy : in_TEXCOORD1.xy;
    vs_TEXCOORD0.zw = u_xlat1.xy * _DetailAlbedoMap_ST.xy + _DetailAlbedoMap_ST.zw;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat1.xyz = in_POSITION0.yyy * hlslcc_mtx4x4unity_ObjectToWorld[1].xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_ObjectToWorld[0].xyz * in_POSITION0.xxx + u_xlat1.xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_ObjectToWorld[2].xyz * in_POSITION0.zzz + u_xlat1.xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat1.xyz;
    vs_TEXCOORD1.xyz = u_xlat1.xyz + (-_WorldSpaceCameraPos.xyz);
    vs_TEXCOORD1.w = 0.0;
    u_xlat2.xyz = in_TANGENT0.yyy * hlslcc_mtx4x4unity_ObjectToWorld[1].xyz;
    u_xlat2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[0].xyz * in_TANGENT0.xxx + u_xlat2.xyz;
    u_xlat2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[2].xyz * in_TANGENT0.zzz + u_xlat2.xyz;
    u_xlat22 = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat22 = inversesqrt(u_xlat22);
    u_xlat2.xyz = vec3(u_xlat22) * u_xlat2.xyz;
    vs_TEXCOORD2.xyz = u_xlat2.xyz;
    u_xlat3.xyz = (-u_xlat1.xyz) * _WorldSpaceLightPos0.www + _WorldSpaceLightPos0.xyz;
    vs_TEXCOORD5.xyz = u_xlat1.xyz;
    vs_TEXCOORD2.w = u_xlat3.x;
    u_xlat1.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat1.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat1.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat22 = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat22 = inversesqrt(u_xlat22);
    u_xlat1.xyz = vec3(u_xlat22) * u_xlat1.xyz;
    u_xlat16_4.xyz = u_xlat2.yzx * u_xlat1.zxy;
    u_xlat16_4.xyz = u_xlat1.yzx * u_xlat2.zxy + (-u_xlat16_4.xyz);
    vs_TEXCOORD4.xyz = u_xlat1.xyz;
    u_xlat1.x = in_TANGENT0.w * unity_WorldTransformParams.w;
    u_xlat16_4.xyz = u_xlat1.xxx * u_xlat16_4.xyz;
    vs_TEXCOORD3.xyz = u_xlat16_4.xyz;
    vs_TEXCOORD3.w = u_xlat3.y;
    vs_TEXCOORD4.w = u_xlat3.z;
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_WorldToLight[1];
    u_xlat1 = hlslcc_mtx4x4unity_WorldToLight[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_WorldToLight[2] * u_xlat0.zzzz + u_xlat1;
    vs_TEXCOORD6 = hlslcc_mtx4x4unity_WorldToLight[3] * u_xlat0.wwww + u_xlat1;
    vs_TEXCOORD7 = vec4(0.0, 0.0, 0.0, 0.0);
    u_xlat16_4.x = dot(in_NORMAL0.xyz, in_NORMAL0.xyz);
    u_xlat16_4.x = inversesqrt(u_xlat16_4.x);
    u_xlat16_4.xyz = u_xlat16_4.xxx * in_NORMAL0.zxy;
    u_xlat16_25 = dot(in_TANGENT0.xyz, in_TANGENT0.xyz);
    u_xlat16_25 = inversesqrt(u_xlat16_25);
    u_xlat16_5.xyz = vec3(u_xlat16_25) * in_TANGENT0.yzx;
    u_xlat16_6.xyz = u_xlat16_4.xyz * u_xlat16_5.xyz;
    u_xlat16_4.xyz = u_xlat16_4.zxy * u_xlat16_5.yzx + (-u_xlat16_6.xyz);
    u_xlat16_4.xyz = u_xlat16_4.xyz * in_TANGENT0.www;
    u_xlat0.xyz = _WorldSpaceCameraPos.yyy * hlslcc_mtx4x4unity_WorldToObject[1].xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToObject[0].xyz * _WorldSpaceCameraPos.xxx + u_xlat0.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToObject[2].xyz * _WorldSpaceCameraPos.zzz + u_xlat0.xyz;
    u_xlat0.xyz = u_xlat0.xyz + hlslcc_mtx4x4unity_WorldToObject[3].xyz;
    u_xlat0.xyz = u_xlat0.xyz + (-in_POSITION0.xyz);
    u_xlat1.y = dot(u_xlat16_4.xyz, u_xlat0.xyz);
    u_xlat1.x = dot(in_TANGENT0.xyz, u_xlat0.xyz);
    u_xlat1.z = dot(in_NORMAL0.xyz, u_xlat0.xyz);
    vs_TEXCOORD8.xyz = u_xlat1.xyz;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es
#ifdef GL_EXT_shader_texture_lod
#extension GL_EXT_shader_texture_lod : enable
#endif

precision highp float;
precision highp int;
#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToShadow[16];
uniform 	mediump vec4 _LightShadowData;
uniform 	vec4 unity_ShadowFadeCenterAndType;
uniform 	vec4 hlslcc_mtx4x4unity_MatrixV[4];
uniform 	mediump vec4 _LightColor0;
uniform 	vec4 _ShadowMapTexture_TexelSize;
uniform 	mediump vec4 _Color;
uniform 	mediump float _Cutoff;
uniform 	float _GlossMapScale;
uniform 	mediump float _Parallax;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToLight[4];
UNITY_LOCATION(0) uniform mediump sampler2D _ParallaxMap;
UNITY_LOCATION(1) uniform mediump sampler2D _MainTex;
UNITY_LOCATION(2) uniform mediump sampler2D _SpecGlossMap;
UNITY_LOCATION(3) uniform highp sampler2D _LightTexture0;
UNITY_LOCATION(4) uniform highp sampler2D _LightTextureB0;
UNITY_LOCATION(5) uniform mediump sampler2DShadow hlslcc_zcmp_ShadowMapTexture;
uniform mediump sampler2D _ShadowMapTexture;
in highp vec4 vs_TEXCOORD0;
in highp vec4 vs_TEXCOORD1;
in highp vec4 vs_TEXCOORD2;
in highp vec4 vs_TEXCOORD3;
in highp vec4 vs_TEXCOORD4;
in highp vec3 vs_TEXCOORD5;
in mediump vec3 vs_TEXCOORD8;
layout(location = 0) out mediump vec4 SV_Target0;
vec3 u_xlat0;
mediump vec4 u_xlat16_0;
vec4 u_xlat1;
mediump vec4 u_xlat16_1;
vec4 u_xlat2;
mediump float u_xlat16_2;
mediump vec3 u_xlat16_3;
vec3 u_xlat4;
mediump float u_xlat16_4;
bool u_xlatb4;
vec3 u_xlat5;
vec4 u_xlat6;
vec4 u_xlat7;
mediump vec3 u_xlat16_8;
mediump vec3 u_xlat16_9;
mediump float u_xlat16_10;
mediump vec2 u_xlat16_11;
vec3 u_xlat15;
mediump float u_xlat16_15;
float u_xlat16;
mediump float u_xlat16_19;
mediump vec3 u_xlat16_20;
mediump float u_xlat16_21;
mediump float u_xlat16_23;
float u_xlat26;
vec2 u_xlat27;
float u_xlat33;
float u_xlat36;
mediump float u_xlat16_36;
bool u_xlatb36;
float u_xlat37;
mediump float u_xlat16_41;
mediump float u_xlat16_42;
void main()
{
    u_xlat16_0.x = dot(vs_TEXCOORD8.xyz, vs_TEXCOORD8.xyz);
    u_xlat16_0.x = inversesqrt(u_xlat16_0.x);
    u_xlat16_11.xy = u_xlat16_0.xx * vs_TEXCOORD8.xy;
    u_xlat16_1.x = vs_TEXCOORD8.z * u_xlat16_0.x + 0.419999987;
    u_xlat16_1.xy = u_xlat16_11.xy / u_xlat16_1.xx;
    u_xlat16_0.x = texture(_ParallaxMap, vs_TEXCOORD0.xy).y;
    u_xlat16_23 = _Parallax * 0.5;
    u_xlat16_23 = u_xlat16_0.x * _Parallax + (-u_xlat16_23);
    u_xlat0.xy = vec2(u_xlat16_23) * u_xlat16_1.xy + vs_TEXCOORD0.xy;
    u_xlat16_1 = texture(_MainTex, u_xlat0.xy);
    u_xlat16_0 = texture(_SpecGlossMap, u_xlat0.xy);
    u_xlat16_2 = u_xlat16_1.w * _Color.w + (-_Cutoff);
    u_xlat16_3.xyz = u_xlat16_1.xyz * _Color.xyz;
#ifdef UNITY_ADRENO_ES3
    u_xlatb36 = !!(u_xlat16_2<0.0);
#else
    u_xlatb36 = u_xlat16_2<0.0;
#endif
    if(((int(u_xlatb36) * int(0xffffffffu)))!=0){discard;}
    u_xlat1 = vs_TEXCOORD5.yyyy * hlslcc_mtx4x4unity_WorldToShadow[1];
    u_xlat1 = hlslcc_mtx4x4unity_WorldToShadow[0] * vs_TEXCOORD5.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_WorldToShadow[2] * vs_TEXCOORD5.zzzz + u_xlat1;
    u_xlat1 = u_xlat1 + hlslcc_mtx4x4unity_WorldToShadow[3];
    u_xlat4.xyz = u_xlat1.xyz / u_xlat1.www;
    u_xlat5.xy = u_xlat4.xy * _ShadowMapTexture_TexelSize.zw + vec2(0.5, 0.5);
    u_xlat5.xy = floor(u_xlat5.xy);
    u_xlat4.xy = u_xlat4.xy * _ShadowMapTexture_TexelSize.zw + (-u_xlat5.xy);
    u_xlat27.xy = (-u_xlat4.xy) + vec2(1.0, 1.0);
    u_xlat6.xy = min(u_xlat4.xy, vec2(0.0, 0.0));
    u_xlat1.xy = (-u_xlat6.xy) * u_xlat6.xy + u_xlat27.xy;
    u_xlat2.y = u_xlat1.x;
    u_xlat27.xy = max(u_xlat4.xy, vec2(0.0, 0.0));
    u_xlat6 = u_xlat4.xxyy + vec4(0.5, 1.0, 0.5, 1.0);
    u_xlat1.xz = (-u_xlat27.xy) * u_xlat27.xy + u_xlat6.yw;
    u_xlat27.xy = u_xlat6.xz * u_xlat6.xz;
    u_xlat2.z = u_xlat1.x;
    u_xlat4.xy = u_xlat27.xy * vec2(0.5, 0.5) + (-u_xlat4.xy);
    u_xlat2.x = u_xlat4.x;
    u_xlat1.x = u_xlat4.y;
    u_xlat2.w = u_xlat27.x;
    u_xlat1.w = u_xlat27.y;
    u_xlat1 = u_xlat1 * vec4(0.444440007, 0.444440007, 0.444440007, 0.222220004);
    u_xlat2 = u_xlat2 * vec4(0.444440007, 0.444440007, 0.444440007, 0.222220004);
    u_xlat6 = u_xlat2.ywyw + u_xlat2.xzxz;
    u_xlat4.xy = u_xlat2.yw / u_xlat6.zw;
    u_xlat4.xy = u_xlat4.xy + vec2(-1.5, 0.5);
    u_xlat2.xy = u_xlat4.xy * _ShadowMapTexture_TexelSize.xx;
    u_xlat7 = u_xlat1.yyww + u_xlat1.xxzz;
    u_xlat4.xy = u_xlat1.yw / u_xlat7.yw;
    u_xlat1 = u_xlat6 * u_xlat7;
    u_xlat4.xy = u_xlat4.xy + vec2(-1.5, 0.5);
    u_xlat2.zw = u_xlat4.xy * _ShadowMapTexture_TexelSize.yy;
    u_xlat6 = u_xlat5.xyxy * _ShadowMapTexture_TexelSize.xyxy + u_xlat2.xzyz;
    u_xlat2 = u_xlat5.xyxy * _ShadowMapTexture_TexelSize.xyxy + u_xlat2.xwyw;
    vec3 txVec0 = vec3(u_xlat6.xy,u_xlat4.z);
    u_xlat16_36 = textureLod(hlslcc_zcmp_ShadowMapTexture, txVec0, 0.0);
    vec3 txVec1 = vec3(u_xlat6.zw,u_xlat4.z);
    u_xlat16_4 = textureLod(hlslcc_zcmp_ShadowMapTexture, txVec1, 0.0);
    u_xlat4.x = u_xlat1.y * u_xlat16_4;
    u_xlat36 = u_xlat1.x * u_xlat16_36 + u_xlat4.x;
    vec3 txVec2 = vec3(u_xlat2.xy,u_xlat4.z);
    u_xlat16_4 = textureLod(hlslcc_zcmp_ShadowMapTexture, txVec2, 0.0);
    vec3 txVec3 = vec3(u_xlat2.zw,u_xlat4.z);
    u_xlat16_15 = textureLod(hlslcc_zcmp_ShadowMapTexture, txVec3, 0.0);
    u_xlat36 = u_xlat1.z * u_xlat16_4 + u_xlat36;
    u_xlat36 = u_xlat1.w * u_xlat16_15 + u_xlat36;
    u_xlat16_4 = (-_LightShadowData.x) + 1.0;
    u_xlat36 = u_xlat36 * u_xlat16_4 + _LightShadowData.x;
    u_xlat16_8.x = (-u_xlat36) + 1.0;
    u_xlat4.xyz = vs_TEXCOORD5.xyz + (-unity_ShadowFadeCenterAndType.xyz);
    u_xlat4.x = dot(u_xlat4.xyz, u_xlat4.xyz);
    u_xlat4.x = sqrt(u_xlat4.x);
    u_xlat15.xyz = (-vs_TEXCOORD5.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat5.x = hlslcc_mtx4x4unity_MatrixV[0].z;
    u_xlat5.y = hlslcc_mtx4x4unity_MatrixV[1].z;
    u_xlat5.z = hlslcc_mtx4x4unity_MatrixV[2].z;
    u_xlat15.x = dot(u_xlat15.xyz, u_xlat5.xyz);
    u_xlat4.x = (-u_xlat15.x) + u_xlat4.x;
    u_xlat4.x = unity_ShadowFadeCenterAndType.w * u_xlat4.x + u_xlat15.x;
    u_xlat4.x = u_xlat4.x * _LightShadowData.z + _LightShadowData.w;
#ifdef UNITY_ADRENO_ES3
    u_xlat4.x = min(max(u_xlat4.x, 0.0), 1.0);
#else
    u_xlat4.x = clamp(u_xlat4.x, 0.0, 1.0);
#endif
    u_xlat16_8.x = u_xlat4.x * u_xlat16_8.x + u_xlat36;
    u_xlat1 = vs_TEXCOORD5.yyyy * hlslcc_mtx4x4unity_WorldToLight[1];
    u_xlat1 = hlslcc_mtx4x4unity_WorldToLight[0] * vs_TEXCOORD5.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_WorldToLight[2] * vs_TEXCOORD5.zzzz + u_xlat1;
    u_xlat1 = u_xlat1 + hlslcc_mtx4x4unity_WorldToLight[3];
    u_xlat4.xy = u_xlat1.xy / u_xlat1.ww;
    u_xlat4.xy = u_xlat4.xy + vec2(0.5, 0.5);
    u_xlat36 = texture(_LightTexture0, u_xlat4.xy).w;
#ifdef UNITY_ADRENO_ES3
    u_xlatb4 = !!(0.0<u_xlat1.z);
#else
    u_xlatb4 = 0.0<u_xlat1.z;
#endif
    u_xlat15.x = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat15.x = texture(_LightTextureB0, u_xlat15.xx).x;
    u_xlat16_19 = (u_xlatb4) ? 1.0 : 0.0;
    u_xlat16_19 = u_xlat36 * u_xlat16_19;
    u_xlat16_19 = u_xlat15.x * u_xlat16_19;
    u_xlat16_8.x = u_xlat16_8.x * u_xlat16_19;
    u_xlat16_8.xyz = u_xlat16_8.xxx * _LightColor0.xyz;
    u_xlat36 = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat36 = inversesqrt(u_xlat36);
    u_xlat4.xyz = vec3(u_xlat36) * vs_TEXCOORD1.xyz;
    u_xlat5.x = vs_TEXCOORD2.w;
    u_xlat5.y = vs_TEXCOORD3.w;
    u_xlat5.z = vs_TEXCOORD4.w;
    u_xlat36 = dot(u_xlat5.xyz, u_xlat5.xyz);
    u_xlat36 = inversesqrt(u_xlat36);
    u_xlat6.xyz = u_xlat5.xyz * vec3(u_xlat36) + (-u_xlat4.xyz);
    u_xlat5.xyz = vec3(u_xlat36) * u_xlat5.xyz;
    u_xlat36 = dot(u_xlat6.xyz, u_xlat6.xyz);
    u_xlat36 = max(u_xlat36, 0.00100000005);
    u_xlat36 = inversesqrt(u_xlat36);
    u_xlat6.xyz = vec3(u_xlat36) * u_xlat6.xyz;
    u_xlat36 = dot(vs_TEXCOORD4.xyz, vs_TEXCOORD4.xyz);
    u_xlat36 = inversesqrt(u_xlat36);
    u_xlat7.xyz = vec3(u_xlat36) * vs_TEXCOORD4.xyz;
    u_xlat36 = dot(u_xlat7.xyz, u_xlat6.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat36 = min(max(u_xlat36, 0.0), 1.0);
#else
    u_xlat36 = clamp(u_xlat36, 0.0, 1.0);
#endif
    u_xlat37 = dot(u_xlat5.xyz, u_xlat6.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat37 = min(max(u_xlat37, 0.0), 1.0);
#else
    u_xlat37 = clamp(u_xlat37, 0.0, 1.0);
#endif
    u_xlat5.x = dot(u_xlat7.xyz, u_xlat5.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat5.x = min(max(u_xlat5.x, 0.0), 1.0);
#else
    u_xlat5.x = clamp(u_xlat5.x, 0.0, 1.0);
#endif
    u_xlat4.x = dot(u_xlat7.xyz, (-u_xlat4.xyz));
    u_xlat33 = (-u_xlat16_0.w) * _GlossMapScale + 1.0;
    u_xlat15.x = u_xlat33 * u_xlat33;
    u_xlat15.x = max(u_xlat15.x, 0.00200000009);
    u_xlat26 = u_xlat15.x * u_xlat15.x;
    u_xlat16 = u_xlat36 * u_xlat26 + (-u_xlat36);
    u_xlat36 = u_xlat16 * u_xlat36 + 1.0;
    u_xlat36 = u_xlat36 * u_xlat36 + 1.00000001e-07;
    u_xlat26 = u_xlat26 * 0.318309873;
    u_xlat36 = u_xlat26 / u_xlat36;
    u_xlat26 = (-u_xlat15.x) + 1.0;
    u_xlat16 = abs(u_xlat4.x) * u_xlat26 + u_xlat15.x;
    u_xlat15.x = u_xlat5.x * u_xlat26 + u_xlat15.x;
    u_xlat15.x = u_xlat15.x * abs(u_xlat4.x);
    u_xlat16_41 = -abs(u_xlat4.x) + 1.0;
    u_xlat4.x = u_xlat5.x * u_xlat16 + u_xlat15.x;
    u_xlat4.x = u_xlat4.x + 9.99999975e-06;
    u_xlat4.x = 0.5 / u_xlat4.x;
    u_xlat36 = u_xlat36 * u_xlat4.x;
    u_xlat36 = u_xlat36 * 3.14159274;
    u_xlat36 = max(u_xlat36, 9.99999975e-05);
    u_xlat36 = sqrt(u_xlat36);
    u_xlat36 = u_xlat5.x * u_xlat36;
    u_xlat16_9.x = dot(u_xlat16_0.xyz, u_xlat16_0.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlatb4 = !!(u_xlat16_9.x!=0.0);
#else
    u_xlatb4 = u_xlat16_9.x!=0.0;
#endif
    u_xlat4.x = u_xlatb4 ? 1.0 : float(0.0);
    u_xlat36 = u_xlat36 * u_xlat4.x;
    u_xlat4.xyz = u_xlat16_8.xyz * vec3(u_xlat36);
    u_xlat16_9.x = (-u_xlat37) + 1.0;
    u_xlat16_20.x = u_xlat16_9.x * u_xlat16_9.x;
    u_xlat16_20.x = u_xlat16_20.x * u_xlat16_20.x;
    u_xlat16_9.x = u_xlat16_9.x * u_xlat16_20.x;
    u_xlat16_20.xyz = (-u_xlat16_0.xyz) + vec3(1.0, 1.0, 1.0);
    u_xlat16_9.xyz = u_xlat16_20.xyz * u_xlat16_9.xxx + u_xlat16_0.xyz;
    u_xlat4.xyz = u_xlat4.xyz * u_xlat16_9.xyz;
    u_xlat16_9.x = max(u_xlat16_0.y, u_xlat16_0.x);
    u_xlat16_9.x = max(u_xlat16_0.z, u_xlat16_9.x);
    u_xlat16_9.x = (-u_xlat16_9.x) + 1.0;
    u_xlat16_9.xyz = u_xlat16_3.xyz * u_xlat16_9.xxx;
    u_xlat16_42 = u_xlat16_41 * u_xlat16_41;
    u_xlat16_42 = u_xlat16_42 * u_xlat16_42;
    u_xlat16_41 = u_xlat16_41 * u_xlat16_42;
    u_xlat16_42 = u_xlat37 + u_xlat37;
    u_xlat16_42 = u_xlat37 * u_xlat16_42;
    u_xlat16_42 = u_xlat16_42 * u_xlat33 + -0.5;
    u_xlat16_41 = u_xlat16_42 * u_xlat16_41 + 1.0;
    u_xlat16_10 = (-u_xlat5.x) + 1.0;
    u_xlat16_21 = u_xlat16_10 * u_xlat16_10;
    u_xlat16_21 = u_xlat16_21 * u_xlat16_21;
    u_xlat16_10 = u_xlat16_10 * u_xlat16_21;
    u_xlat16_42 = u_xlat16_42 * u_xlat16_10 + 1.0;
    u_xlat16_41 = u_xlat16_41 * u_xlat16_42;
    u_xlat0.x = u_xlat5.x * u_xlat16_41;
    u_xlat16_8.xyz = u_xlat0.xxx * u_xlat16_8.xyz;
    u_xlat0.xyz = u_xlat16_9.xyz * u_xlat16_8.xyz + u_xlat4.xyz;
    SV_Target0.xyz = u_xlat0.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif
7                                _ParallaxMap                  _MainTex                _SpecGlossMap                   _LightTexture0                  _LightTextureB0                 _ShadowMapTexture                